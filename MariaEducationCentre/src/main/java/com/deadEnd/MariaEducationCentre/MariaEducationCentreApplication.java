package com.deadEnd.MariaEducationCentre;

import com.deadEnd.MariaEducationCentre.model.User;
import com.deadEnd.MariaEducationCentre.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MariaEducationCentreApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(MariaEducationCentreApplication.class, args);
	}


	@Autowired
	private UserRepository userRepository;


	@Override

	public void run(String... args) throws Exception {
		this.userRepository.save(new User("Md Ariful ", "Islam", "jahorularif@gmail.com"));
		this.userRepository.save(new User("Mesba", "Ul Hekam", "hekambk@gmail.com"));
		this.userRepository.save(new User("John", "Hayun", "john@gmail.com"));
		this.userRepository.save(new User("Mirza", "Shihab", "mirza@gmail.com"));
		this.userRepository.save(new User("Emrul Kayes", "Evan", "evan@gmail.com"));


	}
}
